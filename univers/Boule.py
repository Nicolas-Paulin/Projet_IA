from random import *
from .Serveur import *
import math

# ======================================= CLASS Boule =================================================
class Boule:
	def __init__(self,id):
		self.__id = id
		self.__type=1
		# self.__position = (randint(0,1500),randint(0,1000))
		self.__position = (750,500)
		self.__taille = 100
		self.__couleur = randint(1,7)
# ========================================================================================
	@property
	def id(self):
		return self.__id

	@property
	def position(self):
		return self.__position

	@property
	def taille(self):
		return self.__taille

	@property
	def couleur(self):
		return self.__couleur

	def setPosition(self, position):
		self.__position = position
	def setTaille(self, taille):
		self.__taille = taille

	def grossir(self, taille):
		self.__taille += taille

	def reiniatilise(self):
		self.__position=(0,0)
		self.__taille=5
# ========================================================================================
	def modelisation(self):
		return [self.__position[0], self.__position[1], self.__taille, self.__couleur]

	def update(self, position): #Fonction permettant de faire bouger les boule aléatoirement.
		rapiditer=100
		while position[0] != self.position[0] or position[1]!=self.position[1]:
			if (position[0] > self.position[0]):
				self.setPosition((self.position[0]+rapiditer,self.position[1]))
			elif (position[0] < self.position[0]):
				self.setPosition((self.position[0]-rapiditer,self.position[1]))
			elif (position[1] > self.position[1]):
				self.setPosition((self.position[0],self.position[1]+rapiditer))
			elif (position[1] < self.position[1]):
				self.setPosition((self.position[0],self.position[1]-rapiditer))
			print(self.position)

	def affichage(self):
		print("id de la boule : ",self.id, " à la position : ",self.position," et de taille : ",self.taille)

	def distance(self, b): # permet de tester la distance entre 2 Boules (ou MiniBoules)
		x1 = self.position[0]
		x2 = b.position[0]
		y1 = self.position[1]
		y2 = b.position[1]
		aux=math.sqrt( ((x2-x1)**2) + ((y2-y1)**2) )
		print(x1," ",x2," ",y1," ",y2," ",aux," ",self.taille," ",b.taille)
		return aux

# ======================================== CLASS MiniBoule ===============================================
# Cette classe est destiné à créer les boule de taille fixe qui apparaitront aléatoirement
# sur la map et qui serviront à "nourir" les Boules.

class MiniBoule:
	def __init__(self,id):
		self.__id = id
		self.__position = (randint(0,4000),randint(0,2950))
		self.__taille = 20
		self.__couleur = 0
# ========================================================================================
	@property
	def id(self):
		return self.__id
	@property
	def taille(self):
		return self.__taille
	@property
	def position(self):
		return self.__position

	def setPosition(self, position):
		self.__position = position

	def setTaille(self, taille):
		self.__taille = taille



	def reiniatilise(self):
		self.__position=(randint(0,1000),randint(0,1000)) #Une fois disparu la MiniBoule appele cette méthode et réaparrait sur la map.

# ========================================================================================
	def modelisation(self):
		return [self.__position[0], self.__position[1], self.__taille, self.__couleur]

	def affichage(self):
		print("id de la boule : ",self.id, " à la position : ",self.position," et de taille : ",self.taille)
