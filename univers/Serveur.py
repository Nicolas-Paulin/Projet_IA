#!/usr/bin/env python
# coding: utf-8

from random import *
import threading
from threading import Thread
from .Boule import *
import time

class Serveur(Thread):
    def __init__(self):
        threading.Thread.__init__(self)
        self.__L_Boule=[]
        #Verifier l'etat du serveur
        self.__lancer= False
        self.lock = threading.Lock()
        # nombreBoule = randint(10,30)
        self.id = randrange(1000000)
        nombreBoule=100
        print("un serv")
        for x in range(nombreBoule):
            self.__L_Boule.append(MiniBoule(x))

    def lancer(self):
        return self.__lancer
    def setLancer(self):
        self.__lancer=True


    def evoluerBoule(self):
        if(len(self.__L_Boule)!=0):
            for boule in self.__L_Boule:
                if(isinstance(boule, Boule)):
                    aux = randrange(4)
                    rapiditer=1
                    # switcher = {
                    #     0: boule.position = (boule.position[0]+1,boule.position[1]),
                    #     1: boule.position = (boule.position[0]-1,boule.position[1]),
                    #     2: boule.position = (boule.position[0],boule.position[1]+1),
                    #     3: boule.position = (boule.position[0]+1,boule.position[1]-1),
                    #     }
                    # switcher.get(aux, boule.position[0]+1,boule.position[1])
                    if(aux == 0):
                        boule.setPosition((boule.position[0]+rapiditer,boule.position[1]))
                    elif(aux==1):
                        boule.setPosition((boule.position[0]-rapiditer,boule.position[1]))
                    elif(aux==2):
                        boule.setPosition((boule.position[0],boule.position[1]+rapiditer))
                    elif(aux==3):
                        boule.setPosition((boule.position[0]+1,boule.position[1]-rapiditer))



    def run(self):
        while(True):
            time.sleep(1)
            self.lock.acquire()
            print(str(len(self.__L_Boule))+"    "+str(self.id))
            self.gestionManger()
            self.evoluerBoule()
            self.lock.release()


    def add(self,boule):
        self.lock.acquire()
        self.__L_Boule.append(boule)
        self.lock.release()

    def getBoules(self):
        res=[]
        for boule in self.__L_Boule:
            res.append(boule.modelisation())
        return res

    def getListeBoule(self): #Retourne la liste de Boule
        return self.__L_Boule

    def manger(self, boule, b): # Vérifie la distance entre les deux boule et apelle la fonction disparu pour la Boule qui sera mangée.
        # if boule.taille > (b.taille)*1.5:
        if boule.taille == (b.taille) and isinstance(boule, Boule) and boule.id!=b.id:

            #print(boule.id," bbbllleee"+str(len(self.__L_Boule)))
            dist = boule.distance(b)
            if dist < ((boule.taille*2)/3):
                print("bug incoming       ",dist,"        ",((boule.taille*2)/3))
                boule.setTaille((boule.taille + b.taille/2))
                return b



    def disparu(self, boule): # Supprime la Boule mangée de la liste __L_Boule
        for indice in range(len(__L_Boule)):
            if self.__L_Boule[indice].id == boule.id :
                return indice

    def gestionManger(self):
        if(len(self.__L_Boule)!=0):
            les_mange = []
            for les_mangeur in self.__L_Boule:
                for les_manger in self.__L_Boule:
                    les_mange.append(self.manger(les_mangeur,les_manger))
            for boule_manger in les_mange:
                if(boule_manger!=None):
                    self.__L_Boule.remove(boule_manger)

    # newthread = ClientThread(ip, port, clientsocket)
    # newthread.start()
