from .app import *
from flask import url_for,redirect,render_template , send_from_directory
from .app import db
from flask.ext.login import login_user, current_user, logout_user
from flask import request
from flask.ext.wtf import Form
from wtforms import StringField, HiddenField
from wtforms.validators import DataRequired
from wtforms import PasswordField
from hashlib import sha256
from .models import getClassement
from .models import getTournois
from .models import getGroupe, newProgramme
from .models import getListeJoueurs
from univers.Serveur import *
from univers.Boule import *
import os

from werkzeug import secure_filename


s = Serveur()


							################# HOME ###################
@app.route("/")
def home():
	if current_user.is_authenticated and current_user.type_compte=="utilisateur" :
		return redirect(url_for('acceuil_user'))
	elif current_user.is_authenticated and current_user.type_compte=="administrateur" :
		return redirect(url_for('acceuil_admini'))

	return  render_template(
			"premier-page.html",
			title="Agar.IA")

							################# Login ###################

from hashlib import sha256
from flask.ext.login import login_user, current_user
from .models import User

class UserForm(Form) :
	mail = StringField('ADRESSE MAIL', validators=[DataRequired()])
	nom = StringField('NOM', validators=[DataRequired()])
	prenom = StringField('PRENOM', validators=[DataRequired()])
	pseudo = StringField('PSEUDO', validators=[DataRequired()])
	mdp = PasswordField('MOT DE PASSE', validators=[DataRequired()])
	mdp2 = PasswordField('MOT DE PASSE', validators=[DataRequired()])

	def get_user(pseudo):
		return User.get(pseudo)

@app.route("/inscription/")
def inscription():
	f=UserForm(mail=None,nom=None,prenom=None,pseudo=None,mdp=None,mdp2=None)
	return render_template("inscription.html",title="inscription", form=f)

@app.route("/inscription/", methods=("POST",))
def inscription_user():
	a = None
	f = UserForm()
	if User.query.get(f.pseudo.data)!=None:
		msg = "Votre pseudo est deja pris, merci de le changer. Faites travailler votre imagination !"
		return render_template(
			"inscription.html",
			form=f,
			message=msg)

	if f.validate_on_submit():
		u = User() #Creation du nouvelle instance
		#Ses parametres
		u.pseudo = f.pseudo.data
		u.nom = f.nom.data
		u.prenom = f.prenom.data
		u.mail = f.mail.data
		u.mdp = f.mdp.data

		m = sha256()
		m.update((f.mdp.data).encode())
		u.mdp = m.hexdigest()
		# vérifier si les mot de passe sont les mêmes
		m2 = sha256()
		m2.update((f.mdp2.data).encode())
		u.type_compte="utilisateur"
		u.score=0;
		u.nbDefaites=0;
		u.nbVictoires=0;
		u.img="defaut.jpg"
		if(m.hexdigest() != m2.hexdigest() ):
			msg = "Les mots de passe sont différentes"
			return render_template(
				"inscription.html",
				form=f,
				message=msg)

		#u.usercode = User.query.count()+1
		login_user(u) #je le connecte
		db.session.add(u)
		db.session.commit()

		return render_template('acceuil_user.html',nom=u.pseudo)
	msg = "Un probleme dans vos saisies. Un petit effort, recommencez !"
	return render_template(
		"inscription.html",
		form=f,
		message=msg)

from .models import User

class LoginForm(Form):
	pseudo = StringField("Nom d'utilisateur (pseudo)")
	password = PasswordField('Mot de passe')
	next = HiddenField()

	def get_authenticated_user(self):
		user = User.query.get(self.pseudo.data)
		if user is None :
			return None
		m = sha256()
		m.update(self.password.data.encode())
		passwd = m.hexdigest()
		return user if passwd == user.mdp else None


from flask import request

@app.route("/connexion/", methods=("GET","POST",))
def connexion():
	f = LoginForm()
	if not f.is_submitted() :
		f.next.data = request.args.get("next")
	elif f.validate_on_submit():
		inscrit = f.get_authenticated_user()
		if inscrit is not None :
			login_user(inscrit)
			if(inscrit.type_compte=="utilisateur"):
				next = f.next.data or url_for("acceuil_user")
			else:
				next = f.next.data or url_for("acceuil_admini")
			return redirect(next)
	return render_template(
		"connexion.html",
		form = f,title="connexion")

@app.route("/logout/")
def logout():
	logout_user()
	return redirect(url_for('home'))

							################# Acceuil Utilisateur ###################

@app.route("/acceuil_user")
def acceuil_user():

	return  render_template(
			"acceuil_user.html",
			title="utilisateur")

							################# Acceuil Administrateur ###################

@app.route("/acceuil_admini")
def acceuil_admini():

	return  render_template(
			"acceuil_admini.html",
			title="administrateur")

							################# Apropos ###################

@app.route("/apropos")
def apropos():

	return  render_template(
			"apropos.html",
			title="apropos")

							################# Contact ###################

@app.route("/contact")
def contact():

	return  render_template(
			"contact.html",
			title="contact")

							################# Classements ###################


@app.route("/classements")
def classements():

	return  render_template(
			"classements.html",
			title="classements",
			classements=getClassement())


						################# Bonjour ###################

@app.route("/bonjour")
def bonjour():

	return  render_template(
			"bonjour.html",
			title="bonjour")

						################# Retourner les données du serveur ###################

@app.route("/listeBoules/")
# @app.route("/listeBoules/",methods=['GET'])
def listeBoules():
	if not request.json:
		# abort(400)
		print(str(s.getBoules()[0][3]))
		return str(s.getBoules())

	# return flask.jsonify([(3,4,15),(5,6,15)])
	return flask.jsonify([[3,4,15],[5,6,15]])

						################# jouer ###################

@app.route("/jouer")
def jouer():

	return  render_template(
			"jouer.html",
			title="JOUER")

						################# Module jeu ###################
@app.route("/Canvas/<pseudo>/")
def Canvas(pseudo):
	print(pseudo)
	if s.lancer()==False:
		s.setLancer()
		s.start()
	s.add(Boule(pseudo))
	return render_template(
			"Canvas.html",
			title="Canvas")

						################# NewTournoi ###################

@app.route("/newTournoi")
def newTournoi():
	return  render_template(
			"newTournoi.html",
			title="newTournoi")


						################# NewGroupe ###################

@app.route("/newGroupe")
def newGroupe():
	return  render_template(
			"newGroupe.html",
			title="newGroupe",
			joueurs=getListeJoueurs())

						################# gererGroupe ###################

@app.route("/gererGroupe")
def gererGroupe():
	return  render_template(
			"gererGroupe.html",
			title="gererGroupe",
			groupe=getGroupe())


						################# gererTournoi ###################

@app.route("/gererTournoi")
def gererTournoi():
	return  render_template(
			"gererTournoi.html",
			title="gererTournoi",
			tournoi=getTournois())

						################# lancerJeu ###################

@app.route("/lancerJeu")
def lancerJeu():
	return  render_template(
			"lancerJeu.html",
			title="lancerJeu")

						################# Upload File ###################

@app.route("/nouvelleIa")
def nouvelleIa():
	return  render_template("nouvelleIa.html", title="nouvelleIa", ajouterProgramme = newProgramme)

@app.route("/chargerIa")
def chargerIa():
	return  render_template("chargerIa.html", title="chargerIa")
	
	
						############### Creer Groupe ###################
						
#~ class GroupeForm(Form) :
	#~ membres = StringField('MEMBRES', validators=[DataRequired()])
	#~ nom_groupe = StringField('NOM_GROUPES', validators=[DataRequired()])
	#~ description_groupe = StringField('DESCRIPTION_GROUPE', validators=[DataRequired()])
	#~ pseudo = StringField('PSEUDO', validators=[DataRequired()])
	
	#~ def get_groupe(pseudo):
		#~ return Groupe.get(pseudo)
	
@app.route("/newGroupe/", methods=("GET","POST",))
def creerGroupe():
	#~ g2 = GroupeForm()
	#~ if g2.validate_on_submit():
		
		
		#~ #Ses parametres
		#~ g = Groupe()
		#~ g.nom_groupe = g2.nom_groupe
		#~ g.description_groupe=g2.description_groupe
		#~ g.id_gerant = g2.pseudo
#~ 
		#~ db.session.add(g)
		#~ db.session.commit()
		
		return  render_template(
			"acceuil_admini.html",
			title="administrateur")
