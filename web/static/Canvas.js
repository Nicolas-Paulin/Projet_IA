//initialisation du Canvas
// A modificier pour adapter  voir JSON

    //Serveur la liste doit etre (x,y,vitesse)
var boules = [];
var particleSpeed = 0.5;
var couleurs= ['#00A878',"#009DDC","#FE5E41","#FCBA04","#0496FF","#DB2B39","#011AA9"];
var longeurChemin = 6;

// Deux thread 1= affiche 2= recuperer donner du serveur.


 /*function afficheJR(liste_boules){
  console.log("affichage");
   for(boule : liste_boules)
    console.log("boule : " + boule);


 }*/

 // fonction qui prend la liste du serveur et remplie var boule avec des boules de la liste
function convertir(liste_boule){
  boules = [];
  for(var i = 0;i<liste_boule.length;++i){
    boules.push(new Boule(liste_boule[i][0],liste_boule[i][1],liste_boule[i][2],liste_boule[i][3]));

  }
}


 //FOnction qui met à jours la liste de boule.
function maj(){
  $.getJSON("/listeBoules/",function(data){ convertir(data);});

}



// Creation Canvas
// Le Canvas prend la taille de la fenetre
var windowWidth = window.innerWidth*4;
var windowHeight =  window.innerHeight*4;
var canvas = document.createElement('canvas');
var context = canvas.getContext("2d");
canvas.id = "canvas";
// gestion taille canvas
canvas.width = window.innerWidth*4;
canvas.height = window.innerHeight*4;
document.body.appendChild(canvas);




function Boule (x, y, taille,couleur) {
  //position x ,y pour chaque boule
  this.x = x;
  this.y = y;


  //La taille d'une boule
  this.taille = taille;

  // Pour deplacer une boule Nous avons besoins de ces prochaines x et y ainsi que la vitesse
  this.vel = {
    x : _.random(-20, 20)/100,
    y : _.random(-20, 20)/100,
    min : _.random(2, 10),
    max : _.random(10, 100)/10
  }

  this.entrainer = [];

  this.color = couleurs[couleur];
}

// cette methode affiche les boules sur le Canvas
Boule.prototype.affichage = function() {
  context.beginPath();
  //Specifier la couleur de la boule
  context.fillStyle = this.color;


  // Dessiner la boule
  context.arc(this.x,this.y, this.taille, 0, Math.PI*2);

  var i = this.entrainer.length-1;

  context.fill();
};




// Cette fonction modifier la position des boules
Boule.prototype.modifier = function(){


  var forceDirection = {
    x :  _.random(-1, 1),
    y :  _.random(-1, 1),
  };

  if( Math.abs(this.vel.x + forceDirection.x) < this.vel.max)
    this.vel.x += forceDirection.x;
  if( Math.abs(this.vel.y + forceDirection.y) < this.vel.max)
    this.vel.y += forceDirection.y;

  this.x += this.vel.x*particleSpeed;
  this.y += this.vel.y*particleSpeed;

  if(Math.abs(this.vel.x) > this.vel.min)
      this.vel.x *= 0.99;
  if(Math.abs(this.vel.y) > this.vel.min)
      this.vel.y *= 0.99;

  //entrainer
  this.entrainer.push({
    x : this.x,
    y : this.y
  });

  this.testLongeurPage();

  if(this.entrainer.length > longeurChemin){
    this.entrainer.splice(0,1);
  }
}

// faire en sorte que la boule revient si elle sort de la page
Boule.prototype.testLongeurPage = function() {
  if( this.x > windowWidth + longeurChemin ){
    this.changerPosition(- longeurChemin, "x");
  } else if( this.x < -longeurChemin) {
    this.changerPosition(windowWidth + longeurChemin, "x");
  }
  if( this.y > windowHeight + longeurChemin){
    this.changerPosition(- longeurChemin, "y");
  } else if( this.y < -longeurChemin) {
    this.changerPosition(windowHeight + longeurChemin, "y");
  }
}
Boule.prototype.changerPosition = function(pos, coor) {
  if(coor == "x") {
    this.x = pos;
    _.chain(this.entrainer).each(function(entrainer) {
      entrainer.x = pos;
    });
  } else if( coor == "y" ) {
    this.y = pos;
    _.chain(this.entrainer).each(function(entrainer) {
      entrainer.y = pos;
    });
  }
}


function start(){
	context.clearRect(0,0, canvas.width, canvas.height);
	_.chain(boules).each(function(p, index){
    // timer pour ne pas encombré le serveur.
    p.modifier();
    p.affichage();
	});
  //Refrecher la page
	requestAnimationFrame(start);
}
maj();
var timer=setInterval(maj,10);

start();

// @app.route("/listeBoules/")
// listeBoules():
//    return flask.jsonify([(3,4,15),(5,6,15)])
