from .app import manager, db
from .models import User, Jeu, Programme, Participer, Tournoi, Groupe, AvoirGroupe
from hashlib import sha256

@manager.command
def syncdb():
	'''Creates the tables '''
	
	#Creation de toutes les tables
	db.create_all()	

@manager.command	
def newUser(userpseudo,userNom,userPrenom,userEmail,type_c,password,image = "defaut.jpg"):
	''' Adds a new user '''
	from hashlib import sha256
	m = sha256()
	m.update(password.encode())
	u = User(pseudo=userpseudo,nom=userNom, prenom=userPrenom, mail=userEmail,type_compte=type_c, mdp=m.hexdigest(), score = 0, nbDefaites = 0, nbVictoires = 0, cpp = 0, java = 0, python = 0,img = image)
	db.session.add(u)
	db.session.commit()

@manager.command
def newProgramme(contenu_prog, pseudoUser):
	''' Adds a new Programme '''
	prog = Programme(contenu_programme = contenu_prog, id_user = pseudoUser)
	db.session.add(prog)
	db.session.commit()

@manager.command	
def newTournoi(nom_t, date_d, date_f, id_game, id_mod):
	''' Adds a new tournoi '''
	tournoi = Tournoi(nom_tournoi = nom_t, date_debut = date_d, date_fin = date_f, id_jeu = id_game, id_moderateur = id_mod)
	db.session.add(tournoi)
	db.session.commit()

@manager.command	
def newJeu(nomJeu, descr, obj, contr, pseudoUser, dif):
	''' Adds a new jeu '''
	jeu = Jeu(nom_jeu = nomJeu, description = descr, objectif = obj, contrainte = contr, id_user = pseudoUser, difficulte = dif)
	db.session.add(jeu)
	db.session.commit()

@manager.command	
def newParticiper(id_tourn, id_prog, id_util, s, num_class, a):
	''' Adds a new participant for the tournoi '''
	p = Participer(id_tournoi = id_tourn, id_programme = id_prog, id_user = id_util, score = s, num_classement = num_class, actif = a)
	db.session.add(p)
	db.session.commit()

@manager.command	
def newGroupe(nom_g, gerant):
	''' Adds a new group '''
	groupe = Groupe(nom_groupe = nom_g, id_gerant = gerant)
	db.session.add(groupe)
	db.session.commit()

@manager.command	
def newAvoirGroupe(id_u, id_g):
	''' Adds a new member for the group '''
	membre = AvoirGroupe(id_user = id_u, id_groupe = id_g)
	db.session.add(membre)
	db.session.commit()

@manager.command
def getClassement():
	''' Retourne une liste [(pseudo, score, defaites, victoires, classement)] '''
	query = User.query.all()
	queryTrie = sorted(query, key=getScore)
	liste = []
	i = 1
	for elem in queryTrie:
		liste.append((elem.pseudo, elem.score, elem.nbDefaites, elem.nbVictoires, i))
		i += 1
	return liste

@manager.command		
def getScore(util):
	''' Recupere le score '''
	return -util.score

@manager.command
def setJava(id_user):
	''' Incremente de 1 l'attribut java -> signifie son experience en java '''
	res = User.query.filter(User.pseudo == id_user).first()
	res.java += 1
	db.session.add(res)
	db.session.commit()

@manager.command
def setPython(id_user):
	''' Incremente de 1 l'attribut python -> signifie son experience en python '''
	res = User.query.filter(User.pseudo == id_user).first()
	res.python += 1
	db.session.add(res)
	db.session.commit()

@manager.command
def setCpp(id_user):
	''' Incremente de 1 l'attribut cpp -> signifie son experience en cpp '''
	res = User.query.filter(User.pseudo == id_user).first()
	res.cpp += 1
	db.session.add(res)
	db.session.commit()

@manager.command
def setImg(id_user, image):
	''' modifie change l'image '''
	res = User.query.filter(User.pseudo == id_user).first()
	res.img = image
	db.session.add(res)
	db.session.commit()

@manager.command
def setScore(id_user, sc):
	''' modifie le scrore '''
	res = User.query.filter(User.pseudo == id_user).first()
	sc = int(sc)
	res.score += sc
	db.session.add(res)
	db.session.commit()

@manager.command
def setNbDefaites(id_user):
	''' modifie le nb de défaites '''
	res = User.query.filter(User.pseudo == id_user).first()
	res.nbDefaites += 1
	db.session.add(res)
	db.session.commit()

@manager.command	
def setNbVictoires(id_user):
	''' modifie le nb de victoires '''
	res = User.query.filter(User.pseudo == id_user).first()
	res.nbVictoires += 1
	db.session.add(res)
	db.session.commit()

@manager.command
def getListeAmis(id_util):
	''' renvoie la liste des amis '''
	user = User.query.filter(User.pseudo == id_util).first()
	id_groupes = AvoirGroupe.query.filter(AvoirGroupe.id_user == id_util)
	nb = AvoirGroupe.query.filter(AvoirGroupe.id_user == id_util).count()
	listeAmis = []
	res = []
	if nb >0:
		for elem in id_groupes:
			amis = AvoirGroupe.query.filter(Groupe.id_groupe == elem.id_groupe)
			for a in amis:
				if a not in listeAmis and a.id_user != id_util:
					listeAmis.append(a)
	for element in listeAmis:
		user_ami = User.query.filter(User.pseudo == element.id_user).first()
		res.append(user_ami)
	return res	

@manager.command
def getTournois():
	''' Retourne une liste [(nom_tournois, date_debut, date_fin, nb_participants, nom_moderateur)] '''
	tournois = Tournoi.query.all()   #tous les tournois
	liste = []
	for t in tournois: #t -> tournois
		nb = Participer.query.filter(Participer.id_tournoi == t.id_tournoi).count()
		moderateur = User.query.filter(User.pseudo == t.id_moderateur).first()
		liste.append((t.nom_tournoi, t.date_debut, t.date_fin, nb, moderateur.nom))
	return liste

@manager.command	
def getGroupe():
	''' Retourne une liste [(nom_groupe, nb_membres, nom_moderateur)] '''
	groupes = Groupe.query.all() #tous les groupes
	liste = []
	for g in groupes:
		nb = AvoirGroupe.query.filter(AvoirGroupe.id_groupe == g.id_groupe).count()
		moderateur = User.query.filter(User.pseudo == g.id_gerant).first()
		liste.append((g.nom_groupe, nb, moderateur.nom))
	return liste

@manager.command
def getListeJoueurs():
	''' Retourne tous les joueurs qui ne sont pas administrateurs '''
	liste = []
	joueurs = User.query.all()
	for j in joueurs:
		if j.type_compte == "utilisateur":
			liste.append(j.pseudo)
	return liste
